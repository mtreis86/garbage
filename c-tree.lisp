(defpackage ctree
  (:use :cl))
(in-package :ctree)

;;;; A cheap and dirty implementation of character trees for the purpose of storing things by name

#|
Note that this is set up to use an array as the storage mechanism for the pointers into the
tree. For now. This is not optimal because then all the possible characters, over a million,
will need to be allocated for. Since this is for strings its unlikely ASCII won't cover it tho.
a short hard-coded vector should be faster than a hash-table. But make this a hash table instead
if you have to.
|#

(defvar *dict*)

(defstruct node char contents)

(defun make-ctree (string)
  (loop with tree = (make-node)
        with node = tree
        with length = (length string)
        for index from 0
        for char across string
        do (setf (node-char node) char)
        when (< index length)
          ;; this may look confusing but look again
          do (setf (node-contents node) (make-node)
                   node (node-contents node))
        finally (return tree)))

(defun splice-ctrees (tree1 tree2)
  "Take in two ctrees and return a new tree that combines the two if possible."
  #|
  cases to consider here: (note I'm treating these trees as immutable, always building new ones)
    0: tree1 and tree2 are identical with different contents
    1: tree1 is a subset of tree2 or the other way around
    2: tree1 and tree2 don't share contents
    3: tree1 and tree2 share a sparse assortment of their contents

  first two of these should be obvious.
    0: combine the contents
    1: add the contents of the shorter tree to the larger
  the other two are trickier.
    3: we could just error. Or make an explicit check for splice-ability
    4: this is tricky. I guess I haven't decided on exactly what type of tree this is,
       should it be one big tree with many entry pointers? that could work, I can have multiple
       trees when no characters are shared then connect them eventually once the dict is bigger
       
  |#)
  
