(defpackage props
  (:use :cl))
(in-package :props)

;;;; Constraint propagation of closure networks through pattern matching

#|

Okay so I have been thinking about constraint propagation and what it means to do that. Ignoring
the usual ways this is accomplished, the goal is to have some set of functions that can be applied
to some data. Some of the functions can propagate through the tree, that is they apply to the
function that encapsulates them in the same way they apply themselves.

Consider the following points on a line:
(on-a-line x y z)

Lets say y is to the left of x
(left-of x y)

And y and z are coprime
(coprime y z)

And finally lets say z is 8 away from x
(equal-to 8 (abs (- x z)))

When I say constraints propagate, what I mean is that you can combine any two of these.

Pick any two inputs and we can combine their constraints to produce a new constraint that
has been partially solved and is waiting on the last input.

Let's go through it, why not.











Closure networks have a nice effect where changes in inputs propagate through the network.

An example would be most illustrative. Consider the following transformation:

(let (x y z)
  (xor (z + y)
       (+ (* x y)
          (^ y 3))))

If you change any of the three inputs, x y z, you'll notice that one requires more computation
than the other two. Since z is only used in one half of the xor, we don't need to recalculate
the other half as the answer won't have changed. Something something free variable.

This matches up well with a closure network. If we treat each s-expression as a closure of a
function over its inputs, we get a similarly functional system.

This is also highly parallelizable. Since the flow happens one step at a time it sorta
automatically gets you pipelining if built right.

Keeping state aligned through time could be tricky. We'll see.


Pattern matching comes into place when we go to propagate the constraints. Since it isn't
just data flowing through functions, but functions through functions, we can use pattern
matching to do the transformations.



|#
