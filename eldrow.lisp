(defpackage eldrow
  (:use :cl))
(in-package :eldrow)

#| Okay so here is how I am thinking this can work. I have had a few ideas but this one should be
much faster. Effectively this is a graph search, but manipulating the data such that the structure
makes the graph simple to search is the key to it working. Note that I will build the word chains in
reverse, starting with end stages and working back to possible first words.

There are two basic stages, first discovering what pairs of words have valid transitions, then
searching within those transitions for the longest possible series.

In realty there are more stages than that but that is the mental model I am basing this on. The
basic idea with the additional staging is to load the data in such a way that the last step, the
search, is really just a 'map max' across the set, but since I can check for a max entry as it goes,
that may not even be a step, more like the last step is to check the max.

The words will be loaded in, each as a slot in a structure. One of the other slots will hold an
alist of transition:word. The loaded word structure is the next wordle step and the transition word
is a pointer to the previous word's structure. |#



(defvar *dict-path* #P"~/common-lisp/eldrow/wordle-dict.csv")

(defun load-dict (&optional (path *dict-path*))
  (with-open-file (stream path)
    (loop for entry = (read-line stream nil)
          while entry
          collecting entry)))

(defclass word ()
  ((word :initarg :word :reader :word)
   (transitions :accessor transitions)))

;; (defun all-valid-transitions (word word-list)
;;   (mapcar (lambda (prev) (valid-transitions word prev))
;;           word-list))

#| Use the following as transition chars, - for grey, + for green, and ? for yellow. Then something
like "green yellow yellow grey green" would be "+??-+" |#

(defun valid-transitions (next prev)
  (let ((start (greens next prev)))
    (enumerate-non-greens start)))

(defun greens (next prev)
  (loop for n across next
        for p across prev
        if (char= n p)
          collecting #\+ into transition
        else collecting #\? into transition
        finally (return (coerce transition 'string))))

(defun enumerate-non-greens (string)
  (let ((str-pos-list (string-positions #\? string)))
    (loop for bin from 0 below (expt 2 (length str-pos-list))
          collecting (loop with copy = (copy-seq string)
                           for str-pos in str-pos-list
                           for bin-pos from 0
                           when (plusp (ldb (byte 1 bin-pos) bin))
                             do (setf (aref copy str-pos) #\-)
                           finally (return copy)))))

(defun string-positions (char string)
  (loop for entry across string
        for index from 0
        when (char= char entry)
          collecting index))







