(defpackage gearlist
  (:use :cl))
(in-package :gearlist)

(defparameter thing-pool (make-pool))

(defparameter real-pool (make-pool))

(defun make-pool ()
  (make-hash-table :test #'eq))

(defstruct (thing (:constructor %make-thing))
  properties)

(defstruct (real-thing (:include thing)))

(defun make-thing (properties)
  ())

(defun realize (thing)
  ;; TODO fill in data
  (save-thing thing real-pool))

(defun save-thing (thing pool)
  (setf (gethash thing pool) t))

(defun load-thing (thing pool)
  (gethash thing pool))

