(defpackage fifo
  (:use :cl))
(in-package :fifo)

#|
First in first out buffer wrapping a vector. Two pointers, one for fill one for take.
Will not yet error if you overfill.
|#

(defstruct (f (:constructor %make-f))
  in-pointer out-pointer vector)

(defun make-f (size &key (element-type t) (initial-element nil))
  (%make-f :out-pointer 0 :in-pointer 0 :vector (make-array size
                                                            :fill-pointer nil
                                                            :adjustable nil
                                                            :element-type element-type
                                                            :initial-element initial-element)))

(defun f-in (buffer data)
  "Add one entry of data to the buffer."
  (setf (aref (f-vector buffer) (f-in-pointer buffer)) data)
  ;; currently clobbers, needs error handling here. Or adjustment.
  ;; only an issue if you in more than out
  (let ((in-pointer (f-in-pointer buffer)))
    (setf (f-in-pointer buffer) (if (= (1+ in-pointer) ; TODO once full stop?
                                       (array-dimension (f-vector buffer) 0))
                                    0
                                    (1+ in-pointer))))
  buffer)

(defun f-out (buffer)
  "Pull one entry of data from the buffer."
  (prog1 (aref (f-vector buffer) (f-out-pointer buffer))
    ;; currently doesn't alter the buffer on output
    ;; that means this has a problem where is just keeps dumping the data in a cycle
    ;; if you out more than in
    (let ((out-pointer (f-out-pointer buffer)))
      (setf (f-out-pointer buffer) (if (= (1- out-pointer) ;TODO once empty stop?
                                          (array-dimension (f-vector buffer) 0))
                                       0
                                       (1- out-pointer)))))
  buffer)

(defun f-peek (buffer &optional (out-pointer (f-out-pointer buffer)))
 "Returns the contents of the buffer at the out pointer if supplied, or next entry wrapping."
 (aref (f-vector buffer) out-pointer))
