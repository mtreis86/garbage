(in-package :hc)

;;; Part functions

(defclass part ()
  ((%name :initarg :name :reader name))
  (:documentation
   "A part is any named component."))

(defclass input (part)
  ((%in :initform nil :accessor in))
  (:documentation
   "An input is a marker part within a gate that connects the gate to other
   parts or gates."))

(defclass const (part)
  ((%state :initarg :state :reader state))
  (:documentation
   "A const is a part with a bin-vec as the state that is unchanging such as
   #*0 is binary zero."))

(defclass nand (part)
  ((%in1 :initarg :in1 :reader in1)
   (%in2 :initarg :in2 :reader in2))
  (:documentation
   "A nand is the base-most logical component upon which all other logical
   components are built.
   0 0 => 1
   0 1 => 0
   1 0 => 0
   1 1 => 0"))

(defclass gate (part)
  ((%funct :initform nil :accessor funct)
   (%struct :initform (make-hash-table) :accessor struct)
   (%output :initform nil :accessor output)
   (%inputs :initform nil :accessor inputs))
  (:documentation
   "A gate is an assembly of parts. The structure holds the parts and their
   connections, the funct is a compiled function that can be called to
   output a state based on the inputs to the gate. Output is set to the part
   whose state represents the state of the entire gate."))

(defgeneric make-part (name part-type &key &allow-other-keys)
  (:documentation
   "Initialize a part of the given name, with the required args being based on
   the part-type. The following table lists the type-arg relationship.
   Blank means no args required. Notes on the args are in parans.
   input:
   gate:
   const: state    (state must be a bit-vec)
   nand: in1 in2   (in1 and in2 are symbols naming other parts)"))

(defmethod make-part (name (part-type (eql :input)) &key)
  "Input part initialization."
  (make-instance 'input
                 :name name))

(defmethod make-part (name (part-type (eql :const)) &key state)
  "Const part initialization."
  (make-instance 'const
                 :name name
                 :state state))

(defmethod make-part (name (part-type (eql :nand)) &key in1 in2)
  "Nand part initialization."
  (make-instance 'nand
                 :name name
                 :in1 in1
                 :in2 in2))

(defmethod make-part (name (part-type (eql :gate)) &key)
  "Gate part initialization."
  (make-instance 'gate
                 :name name))

(defmacro make-parts (part-list)
  "Define multiple parts at a time."
  (let ((parts (loop for part in part-list
                     collecting `(make-part ,@part))))
    `(list ,@parts)))

;; (defun make-parts (part-list)
;;   "Define multiple parts at a time.")

(defun get-part (gate name)
  "Return the part with the given name in the struct of the gate."
  (gethash name (struct gate)))

(defmacro get-parts (gate name-list)
  "Get multiple parts and return a list of them."
  (let ((parts (loop for name in name-list
                     collecting `(get-part ,gate ,name))))
    `,parts))

(defun set-part (gate part)
  "Add the part to the gate's structure."
  (setf (gethash (name part) (struct gate)) part))

(defmacro set-parts (gate part-list)
  "Add multiple parts to the gate's structure."
  (let ((part (gensym "PART")))
    `(dolist (,part ,part-list)
       (set-part ,gate ,part))))

(defun compile-gate (gate output-part)
  "Take the structure of the gate, with one part identified as the output, and
  set the funct of the gate to a function that can be called to return the state
  of the gate dependant upon the parts connected to the inputs."
  (setf (output gate) output-part)
  (labels ((sub-compile (name)
             (let ((part (get-part gate name)))
                 (etypecase part
                   (const
                    (let ((state (state part)))
                      (lambda () state)))
                   (input
                    (push part (inputs gate))
                    (let ((in (in part)))
                      (if (null in)
                          (lambda () (in (get-part gate name)))
                          (lambda () in))))
                   (nand
                    (let ((in1 (sub-compile (in1 part)))
                          (in2 (sub-compile (in2 part))))
                      (lambda () (bit-nand
                                    (funcall in1)
                                    (funcall in2)))))
                   (gate
                    ;; TODO fixme
                    ;; the conditional here is broken, the lambda after it works
                    ;; gate doesn't compile or something
                    (unless (funct part)
                      (compile-gate part)) 
                    (lambda () (run-gate part)))))))
    (setf (funct gate) (sub-compile output-part))
    gate))

(defun get-inputs (gate name)
  "Return a list of the inputs of the part by name within the gate struct."
  (let ((part (get-part gate name)))
    (etypecase part
      (nand '(in1 in2))
      (gate (inputs part)))))

(defun set-input (gate name input-part)
  "Connect the input by name within the gate-struct to the given input-part."
  (let ((part (get-part gate name)))
    (setf (in part) input-part)))

;; and :nand not :in
(defun link-input (gate part-name input-part input-name)
  "Connect within the gate the input part's named input to the part."
  (set-input input-part input-name (get-part gate part-name)))

(defun set-output (gate name)
  "Set which part is to be used as the output of the gate when the funct is
  called."
  (setf (output gate) name))

(defmacro run-gate (gate)
  "Call the function representing the gate and return the output."
  `(funcall (funct ,gate)))



;;; Tests

(deftest test-parts ()
  (combine-results
    (test-not)
    (test-xor4)))

(deftest test-not ()
  (let ((gate (make-part :not :gate))
        (part-list (make-parts ((:in :input)
                                (:one :const :state #*1)
                                (:nand :nand :in1 :in :in2 :one))))
        (zero #*0)
        (one #*1))
    (set-parts gate part-list)
    (compile-gate gate :nand)
    ;; tests
    (set-input gate :in one)
    (check (bit-eqv (run-gate gate) (bit-not one)))
    (set-input gate :in zero)
    (check (bit-eqv (run-gate gate) (bit-not zero)))))

(deftest test-xor4 ()
  (let ((gate (make-part :xor :gate))
        (part-list (make-parts ((:in1 :input)
                                (:in2 :input)
                                (:nand1 :nand :in1 :in1 :in2 :in2)
                                (:nand2 :nand :in1 :in1 :in2 :nand1)
                                (:nand3 :nand :in1 :in2 :in2 :nand1)
                                (:nand4 :nand :in1 :nand2 :in2 :nand3))))
        (zero #*0)
        (one #*1))
    (set-parts gate part-list)
    (compile-gate gate :nand4)
    ;; tests
    (set-input gate :in1 zero)
    (set-input gate :in2 zero)
    (check (bit-eqv (run-gate gate) (bit-xor zero zero)))
    (set-input gate :in1 one)
    (check (bit-eqv (run-gate gate) (bit-xor one zero)))
    (set-input gate :in2 one)
    (check (bit-eqv (run-gate gate) (bit-xor one one)))
    (set-input gate :in1 zero)
    (check (bit-eqv (run-gate gate) (bit-xor zero one)))))





;; (deftest test-xor5 ()
;;   (let ((xor (make-part :xor :gate))
;;         (or (make-part :or :gate))
;;         (and (make-part :and :gate))
;;         (not (make-part :not :gate))
;;         (or-parts (make-parts ((:in1 :input)
;;                                (:in2 :input)
;;                                (:nand1 :nand :in1 :in1 :in2 :in2)
;;                                (:nand2 :nand :in1 :nand1 :in2 :nand1))))
;;         (not-parts (make-parts ((:in :input)
;;                                 (:one :const :state #*1)
;;                                 (:nand :nand :in1 :in :in2 :one)))))
;;     (set-parts or or-parts)
;;     (set-parts not not-parts)
;;     (compile-gate or :nand2)
;;     (compile-gate not :nand)
;;     (let ((and-parts (make-parts ((:in1 :input)
;;                                   (:in2 :input)
;;                                   (:nand :nand :in1 :in1 :in2 :in2)))))
;;       (set-parts and and-parts)
;;       (set-part and not)
;;       (compile-gate and :not)
;;       (let ((xor-parts (make-parts ((:in1 :input)
;;                                     (:in2 :input))))
;;             (zero #*0)
;;             (one #*1))
;;         (set-parts xor xor-parts)
;;         (set-part xor and)
;;         (set-part xor or)
;;         (set-part xor (make-part :nand :nand :in1 :and :in2 :or))
;;         (compile-gate xor :nand)
;;         ;; tests
;;         (set-input xor :in1 zero)
;;         (set-input xor :in2 zero)
;;         xor))))
        ;; (check (bit-eqv (run-gate xor) (bit-xor zero zero)))
        ;; (set-input xor :in1 one)
        ;; (check (bit-eqv (run-gate xor) (bit-xor one zero)))
        ;; (set-input xor :in2 one)
        ;; (check (bit-eqv (run-gate xor) (bit-xor one one)))
        ;; (set-input xor :in1 zero)
        ;; (check (bit-eqv (run-gate xor) (bit-xor zero one)))))))







