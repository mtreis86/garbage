(in-package :hc)

;;; Part functions

(defclass part ()
  ((%name :initarg :name :reader name))
  (:documentation
   "A part is any named component."))

(defclass input (part)
  ((%in :initform nil :accessor in))
  (:documentation
   "An input is a marker part within a gate that connects the gate to other
   parts or gates."))

(defclass const (part)
  ((%state :initarg :state :reader state))
  (:documentation
   "A const is a part with a bin-vec as the state that is unchanging such as
   #*0 is binary zero."))

(defclass nand (part)
  ((%in1 :initarg :in1 :reader in1)
   (%in2 :initarg :in2 :reader in2))
  (:documentation
   "A nand is the base-most logical component upon which all other logical
   components are built.
   0 0 => 1
   0 1 => 0
   1 0 => 0
   1 1 => 0"))

(defclass gate (part)
  ((%funct :initform nil :accessor funct)
   (%struct :initform (make-hash-table) :reader struct)
   (%output :initform nil :accessor output))
  (:documentation
   "A gate is an assembly of parts. The structure holds the parts and their
   connections, the funct is a compiled function that can be called to
   output a state based on the inputs to the gate. Output is set to the part
   whose state represents the state of the entire gate."))

(defmacro defpart (name input-count)
  "Create a class and a make-part method for the named part with the number of
  input-counts specified."
  (with-gensyms (slot-list input-list instance-list)
    (let ((key-name (intern (format nil "~A" name) :keyword)))
      (loop for num from 1 to input-count
            do (let ((slot (intern (format nil "~A~A" '%in num)))
                     (initarg (intern (format nil "~A~A" 'in num) :keyword))
                     (accessor (intern (format nil "~A~A" 'in num))))
                 (setf slot-list (append slot-list
                                         `((,slot :initarg ,initarg
                                                  :accessor ,accessor)))
                       input-list (append input-list (list accessor))
                       instance-list (append instance-list
                                             `(,initarg ,accessor)))))
      `(progn (defclass ,name (gate)
                (,@slot-list))
              (defmethod make-part (name (part-type (eql ,key-name))
                                    &key ,@input-list)
                (make-instance ',name
                               :name name
                               ,@instance-list))))))

(defmacro with-defpart (name input-count &body body)
  `(progn (define-part ,name ,input-count)
          ,@body))

(defgeneric make-part (name part-type &key &allow-other-keys)
  (:documentation
   "Initialize a part of the given name, with the required args being based on
   the part-type. The following table lists the type-arg relationship.
   Blank means no args required. Notes on the args are in parans.
   input:
   gate:
   const: state    (state must be a bit-vec)
   nand: in1 in2   (in1 and in2 are symbols naming other parts)"))

(defmethod make-part :before (name part-type &key)
  (dbg 'hc "Making part:~% Name: ~A~% Type: ~A~%" name part-type))

(defmethod make-part (name (part-type (eql :input)) &key)
  "Input part initialization."
  (make-instance 'input
                 :name name))

(defmethod make-part (name (part-type (eql :const)) &key state)
  "Const part initialization."

  (make-instance 'const
                 :name name
                 :state state))

(defmethod make-part (name (part-type (eql :nand)) &key in1 in2)
  "Nand part initialization."
  (make-instance 'nand
                 :name name
                 :in1 in1
                 :in2 in2))

(defmethod make-part (name (part-type (eql :gate)) &key)
  "Gate part initialization."
  (make-instance 'gate
                 :name name))

(defmacro make-parts (part-list)
  "Define multiple parts at a time."
  (let ((parts (loop for part in part-list
                     collecting `(make-part ,@part))))
    `(list ,@parts)))

(defun get-part (gate name)
  "Return the part with the given name in the struct of the gate."
  (gethash name (struct gate)))

(defmacro get-parts (gate name-list)
  "Get multiple parts and return a list of them."
  (let ((parts (loop for name in name-list
                     collecting `(get-part ,gate ,name))))
    `,parts))

(defun set-part (gate part)
  "Add the part to the gate's structure."
  (dbg 'hc "Setting ~A part into ~A gate" (name part) (name gate))
  (setf (gethash (name part) (struct gate)) part))

(defmacro set-parts (gate part-list)
  "Add multiple parts to the gate's structure."
  (let ((part (gensym "PART")))
    `(dolist (,part ,part-list)
       (set-part ,gate ,part))))

(defgeneric compile-part (gate part)
  (:documentation
   "Take the structure of the gate, and, based on the part type, compile the
   contents of the gate's struct starting at the output and working towards
   inputs and consts until the complete functionality of the gate is captured in
   closures. Set the funct of the gate to that function and return the gate."))

(defmethod compile-part :before ((gate gate) (part const))
  (dbg 'hc " Compiling: ~A~%  State: ~A~%" (name part) (state part)))

(defmethod compile-part ((gate gate) (part const))
  (let ((state (state part)))
    (lambda () state)))

(defmethod compile-part :before ((gate gate) (part input))
  (dbg 'hc " Compiling: ~A~%  In: ~A~%" (name part) (in part)))

(defmethod compile-part ((gate gate) (part input))
  (lambda () (in part)))

(defmethod compile-part :before ((gate gate) (part nand))
  (dbg 'hc " Compiling: ~A~%  In1: ~A~%  In2: ~A~%"
       (name part) (in1 part) (in2 part)))

(defmethod compile-part ((gate gate) (part nand))
  (let ((in1 (compile-part gate (get-part gate (in1 part))))
        (in2 (compile-part gate (get-part gate (in2 part)))))
    (lambda () (bit-nand
                  (funcall in1)
                  (funcall in2)))))

(defmethod compile-part :before ((gate gate) (part gate))
  (dbg 'hc "Compiling gate: ~A" (name gate))
  (unless (output part)
    (error "Part doesn't have an output")))

(defmethod compile-part ((gate gate) (part gate))
  (unless (funct part)
    (setf (funct part) (compile-part part (get-part part (output part)))))
  (lambda () (funcall (funct part))))

(defun compile-gate (gate)
  (compile-part gate gate))

(defun set-input (gate name input-part)
  "Connect the input by name within the gate-struct to the given input-part."
  (let ((part (get-part gate name)))
    (setf (in part) input-part)))

;; and :nand not :in
(defun link-input (gate part-name input-part input-name)
  "Connect within the gate the input part's named input to the part."
  (set-input input-part input-name (get-part gate part-name)))

(defun set-output (gate name)
  "Set which part is to be used as the output of the gate when the funct is
  called."
  (setf (output gate) name))

(defun run-gate (gate)
  "Call the function representing the gate and return the output."
  (funcall (funct gate)))








