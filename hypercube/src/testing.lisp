;;;;Testing
;;;;From PCL

(in-package #:hc)

;;; Example of tests in use:
;; (deftest test-+ ()
;;   (check
;;     (= (+ 1 2) 3)
;;     (= (+ 1 2 3) 6)
;;     (= (+ -1 -3) -4)))
;; (deftest test-* ()
;;   (check
;;     (= (* 2 2) 4)
;;     (= (* 3 5) 15)
;;     ;; this one should fail
;;     (= (* -1 2) -1)))
;; (deftest test-arithmetic ()
;;   (combine-results
;;     (test-+)
;;     (test-*)))

(defvar *test-name* nil "Holds tests defined by testing functions.")

(defmacro with-gensyms ((&rest names) &body body)
  "Insert a let statement for each name utilizing a gensym to avoid symbol clash."
  `(let ,(loop for n in names collect `(,n (gensym)))
     ,@body))

(defmacro deftest (name parameters &body body)
  "Setup a test using standard defun style form."
  `(defun ,name ,parameters
     (let ((*test-name* (append *test-name* (list ',name))))
       ,@body)))

(defmacro check (&body forms)
  "Process tests, each test must result in t/nil based on whether it passes or not."
  `(combine-results
     ,@(loop for f in forms collect `(report-result ,f ',f))))

(defmacro combine-results (&body forms)
  "Collect checks and assign them a name such that a heirarchy can be established. Eg. test-numbers could be the collection of test-odds and test-evens."
  (with-gensyms (result)
    `(let ((,result t))
       ,@(loop for f in forms collect `(unless ,f (setf ,result nil)))
       ,result)))

(defun report-result (result form)
  "Print results of tests in a pass-fail format, including the test name."
  (format t "~:[FAIL~;pass~] ... ~a: ~a~%" result *test-name* form)
 result)
