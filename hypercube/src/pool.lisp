(in-package :hc)


;;; Pool functions

(defclass pool ()
  ((%pool :initform (make-hash-table) :reader pool)))

(defun make-pool ()
  (make-instance 'pool))

(defun save-gate (gate pool)
  (setf (gethash (name gate) (pool pool)) gate))

(defun get-gate (name pool)
  (copy-gate (gethash name (pool pool))))

(defun copy-gate (gate)
  "Copy the structure of the gate. Doesn't copy inputs."
  (let ((new (make-part (name gate) :gate)))
    (setf (funct new) (funct gate)
          (output new) (output gate))
    (copy-hash-table (struct gate) (struct new))
    new))

(defun copy-hash-table (old new)
  (maphash (lambda (key val)
             (setf (gethash key new) val))
           old))

(defun set-gate (gate pool name &rest inputs)
  "Add the named gate from the pool to gate's structure setting inputs."
  (let ((part (copy-gate (get-gate name pool))))
    (setf (gethash name (struct gate)) part)
    (loop for (sub-name sub-part) in inputs
          do (setf (gethash sub-name (struct part)) (get-part gate sub-part)))
    part))
