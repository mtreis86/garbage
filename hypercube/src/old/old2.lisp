(in-package #:hc)

;;;; Michael Reis
;;;; <michael@mtreis86.com>

#| This is the start of a game I have been thinking about for a while. One
aspect of it anyways. The basic idea is to have a couple layers to play puzzles
in where the puzzles are basically building a computer at various levels.
One level would be nands, the next would maybe be lambda calculus, the last
would be a lisp built on top of the other two. Or maybe just nands and lisp.
This is the simulator for that system.

I started this by making it CLOS but found it to be too difficult to understand
what I was doing and since CLOS isn't much more than a set of macros on top
of closures and typecases, I am just going to write it in regular lisp. This
should be faster anyways.
|#



(defstruct (part (:constructor %make-part))
  (name nil :type symbol)
  (type 'part :type symbol)
  (sources (make-hash-table) :type hash-table)
  (sinks (make-hash-table) :type hash-table)
  (parts (make-hash-table) :type hash-table)
  (state #*0 :type simple-bit-vector))

(defstruct (ram (:constructor %make-ram))
  (name nil :type symbol)
  (type 'ram :type symbol)
  (state #*0 :type simple-bit-vector))

(defstruct (pool (:constructor %make-pool))
  (parts (make-hash-table) :type hash-table))

(defun make-part (&key type)
  "Instanciate a new, empty, part. If a type is provided, set the part-type to
  that. Should not be used exposed to users as it doesn't protect against making
  the same type of part twice, or trampling over the built in types. Users
  should use the make function."
  (let ((part (%make-part)))
    (when type (setf (part-type part) type))
    part))

(defun make-pool ()
  "Instanciate a new pool for collecting part templates. Put built-in parts into
  the pool."
  (let ((pool (%make-pool)))
    (setf (gethash 'nand (pool-parts pool)) (make-nand)
          (gethash 'input (pool-parts pool)) (make-input)
          (gethash 'output (pool-parts pool)) (make-output)
          (gethash 'const (pool-parts pool)) (make-const))
    pool))

;;; Built in part initialization
(let ((nand (make-part :type 'nand))
      (input (make-part :type 'input))
      (output (make-part :type 'output))
      (const (make-part :type 'const)))
  (defun make-input ()
    "Make a copy of the input type of part. Shouldn't be used by users."
    (let ((input (copy-structure input)))
      (setf (gethash 'in (part-sources input)) t)
      input))
  (defun make-output ()
    "Make a copy of the output type of part. Shouldn't be used by users."
    (let ((output (copy-structure output)))
      (setf (gethash 'out (part-sinks output)) t)
      output))
  (defun make-const (&optional number)
    "Make a copy of the const type of part and if a number is provided set the
    state of the const to a bit array in twos complement that is the binary
    version of that number. Shouldn't be used by users."
    (let ((const (copy-structure const)))
      (setf (gethash 'out (part-sinks const)) (make-output))
      (when number
        (setf (part-state const) (int->bit-vec number)))
      const))
  (defun make-nand ()
    "Make a copy of the nand type of part. Shouldn't be used by users."
    (let ((nand (copy-structure nand)))
      (setf (gethash 'in1 (part-sources nand)) (make-input)
            (gethash 'in2 (part-sources nand)) (make-input)
            (gethash 'out (part-sinks nand)) (make-output))
      nand)))

;;; Pool functions
(let ((pool (make-pool)))
  (defun make (type)
    "Add a new part to the pool. Will error if a part of that type already
    exists."
    (if (gethash type (pool-parts pool))
        (error "~A already exists." type)
        (let ((new-part (make-part :type type)))
          (setf (gethash type (pool-parts pool)) new-part)
          new-part)))
  (defun edit (type)
    "Take a part in the pool and open it for editing. Will error if the part is
    one of the built-in types."
    (let ((part (gethash type (pool-parts pool))))
      (cond ((null part)
             (error "~A type of part not found." type))
            ((or (eql type 'input)
                 (eql type 'output)
                 (eql type 'nand)
                 (eql type 'const))
             (error "~A is built-in and cannot be edited." type))
            (t (edit-part part)))))
  (defun drop (type)
    "Remove a part from the pool."
    (cond ((null (gethash type (pool-parts pool)))
           (error "~A not found in pool" type))
          ((or (eql type 'input)
               (eql type 'nand)
               (eql type 'output)
               (eql type 'const))
           (error "Can only remove user made types from pool."))
          (t (remhash type (pool-parts pool))))
    pool)
  (defun print-pool ()
    "Prints the parts in the pool."
    (format t "Parts in pool:~%")
    (maphash (lambda (type name)
               (declare (ignore name))
               (format t "~A" type))
             (pool-parts pool))
    pool)
  (defun get-pool ()
    "Return the pool."
    pool)
  (defun clear-pool ()
    (setf pool (make-pool))))


;;; Part functions
(let ((part nil))
  (defun edit-part (incoming-part)
    "Set the part to be edited. Shouldn't be called by users."
    (setf part incoming-part)
    part)
  (defun stop ()
    "Stop editing a part."
    (setf part nil))
  (defun update-nand ()
    "Update the nand by looking at the inputs' states and nanding them."
    (declare (optimize (debug 3) (speed 0)))
    (if (eql (part-type part) 'nand)
        (let ((new-state (bit-nand
                          (part-state (gethash 'in1 (part-sources part)))
                          (part-state (gethash 'in2 (part-sources part))))))
          (dbg 'hc "Setting new state to ~A~%" new-state)
          (set-state-vec new-state))
        (error "~A is not a nand." part)))
  (defun grab (type name &optional number)
    "Make and return a copy of some part type with the specified name."
    (if (null (gethash type (pool-parts (get-pool))))
        (error "~A part not found." type)
        (let ((new-part (case type
                          (nand (make-nand))
                          (input (make-input))
                          (output (make-output))
                          (const (make-const number))
                          (t (make-part :type type)))))
          (setf (part-name new-part) name
                (gethash name (part-parts part)) new-part)
          (cond ((or (eql type 'input) (eql type 'const))
                 (setf (gethash name (part-sources part)) new-part))
                ((eql type 'output)
                 (setf (gethash name (part-sinks part)) new-part)))
          new-part)))
  (defun set-state (bit)
    "Set the part to the binary state. Expects a 1 or 0 as the input."
    (setf (part-state part) (int->bit-vec bit)))
  (defun set-state-vec (bit-vec)
    "Set the state of the part manually. Expects a simple bit vector
    #*0 or #*1."
    (setf (part-state part) bit-vec)
    (dbg 'hc "State set to: ~A~%" (part-state part)))
  (defun set-input-state (input bit) ; ONLY FOR TESTING
    "Set the state of the input of the part as if it had been updated."
    (dbg 'hc "Input: ~A~%Bit: ~A~%~%"input bit)
    (setf (part-state (gethash input (part-sources part)))
          (int->bit-vec bit)))
  (defun count-nands ()
    "Count the number of nands in the part."
    (let ((count 0))
      (walk-nand-tree (lambda () (incf count)))
      count))
  (defun view ()
    "Print the part and its internal structure."
    (format t "Part: ~A~%Type: ~A~%Name: ~A~%"
            part (part-type part) (part-name part))
    (maphash (lambda (key val)
               (declare (ignore val))
               (format t "Source: ~A~%" key)) (part-sources part))
    (maphash (lambda (key val)
               (declare (ignore val))
               (format t "Sink: ~A~%" key)) (part-sinks part))
    (maphash (lambda (key val)
               (declare (ignore val))
               (format t "Sub-part: ~A~%" key)) (part-parts part)))
  (defun link (from-part from-sink to-part to-source)
    "Connect the from and to parts to eachother. Similar to a doublely
    linked list."
    (cond ((null (gethash to-part (part-parts part)))
           (error "~A not in part, grab it first." to-part))
          ((null (gethash from-part (part-parts part)))
           (error "~A not in part, grab it first." from-part))
          (t (setf (get-sink (get-part from-part) from-sink) (get-part to-part)
                   (get-source (get-part to-part) to-source) (get-part from-part))
             part)))
  (defun get-part (&optional sub-part)
    "Return the active part, or the sub-part if specified. Sub part needs to be
    located within part-parts of the part."
    (if sub-part
        (gethash sub-part (part-parts part))
        part))
  (defun test () ;; TODO
    "Return a binary test table for the part."
    ())
  ;; (defun map-nands (fn) ; TODO
  ;;   "Follows the part's nands from input to output doing fn in the process.
  ;;   Typical use-case would be to incf a counter or update the nand."
  ;;   ())
  (defun update-part ()
    "Update the part by tracting through the nands contained within it, setting
    their states based on their inputs, and finally setting the part state based
    on their resulting outputs."
    (map-nands #'update-nand))

  (defun map-nands (fn)))





(defun map-sinks (part fn)
  (maphash (lambda (name part)
             (dbg 'hc "Mapping over sinks in ~A, current: ~A~%"
                  (part-name part) name)
             (funcall fn part))
           (part-sinks part)))

(defun map-sources (part fn)
  (maphash (lambda (name part)
             (dbg 'hc "Mapping over sources in ~A, current: ~A~%"
                  (part-name part) name)
             (funcall fn part))
           (part-sources part)))

(deftest test-xor ()
  (clear-pool)
  (make 'xor)
  (edit 'xor)
  (grab 'nand 'nand1)
  (grab 'nand 'nand2)
  (grab 'nand 'nand3)
  (grab 'nand 'nand4)
  (grab 'input 'in1)
  (grab 'input 'in2)
  (grab 'output 'out)
  (link 'in1 'in 'nand1 'in1)
  (link 'in1 'in 'nand3 'in1)
  (link 'in2 'in 'nand3 'in2)
  (link 'in2 'in 'nand2 'in1)
  (link 'nand3 'out 'nand1 'in2)
  (link 'nand3 'out 'nand2 'in2)
  (link 'nand1 'out 'nand4 'in1)
  (link 'nand2 'out 'nand4 'in2)
  (link 'nand4 'out 'out 'out)
  (set-input-state 'in1 1)
  (set-input-state 'in2 1)
  (map-sinks (get-part) #'print))


;; (update-part)
  ;; (check (equalp (part-state (get-part)) #*0))
  ;; (get-part))



(defun get-sub-part (part name)
  (gethash name (part-parts part)))

(defun (setf get-sub-part) (arg part name)
  (setf (gethash name (part-parts part)) arg))

(defun get-source (part name)
  (gethash name (part-sources part)))

(defun (setf get-source) (arg part name)
  (setf (gethash name (part-sources part)) arg))

(defun get-sink (part name)
  (gethash name (part-sinks part)))

(defun (setf get-sink) (arg part name)
  (setf (gethash name (part-sinks part)) arg))


;;; Tests


(deftest test-integration ()
  (test-nand)
  (test-not))

(deftest test-not ()
  (clear-pool)
  (make 'not)
  (edit 'not)
  (grab 'nand 'nand1)
  (grab 'const 'one 1)
  (grab 'input 'in)
  (grab 'output 'out)
  (link 'one 'out 'nand1 'in2)
  (link 'in 'in 'nand1 'in1)
  (set-input-state 'in 1)
  (edit-part (get-part 'nand1))
  (update-nand)
  (check (equalp (part-state (get-part)) #*0))
  (edit 'not)
  (set-input-state 'in 0)
  (edit-part (get-part 'nand1))
  (update-nand)
  (check (equalp (part-state (get-part)) #*1)))

(deftest test-nand ()
  (let ((zero #*0)
        (one #*1)
        (nand (make-nand)))
    (edit-part nand)
    ;; 0 0 -> 1
    (set-input-state 'in1 0)
    (set-input-state 'in2 0)
    (update-nand)
    (check
      (equalp (part-state (gethash 'in1 (part-sources nand))) zero)
      (equalp (part-state (gethash 'in2 (part-sources nand))) zero)
      (equalp (part-state nand) one))
    ;; 0 1 -> 1
    (set-input-state 'in1 0)
    (set-input-state 'in2 1)
    (update-nand)
    (check
      (equalp (part-state (gethash 'in1 (part-sources nand))) zero)
      (equalp (part-state (gethash 'in2 (part-sources nand))) one)
      (equalp (part-state nand) one))
    ;; 1 0 -> 1
    (set-input-state 'in1 1)
    (set-input-state 'in2 0)
    (update-nand)
    (check
      (equalp (part-state (gethash 'in1 (part-sources nand))) one)
      (equalp (part-state (gethash 'in2 (part-sources nand))) zero)
      (equalp (part-state nand) one))
    ;; 1 1 -> 0
    (set-input-state 'in1 1)
    (set-input-state 'in2 1)
    (update-nand)
    (check
      (equalp (part-state (gethash 'in1 (part-sources nand))) one)
      (equalp (part-state (gethash 'in2 (part-sources nand))) one)
      (equalp (part-state nand) zero))))




;;; Utilities

(defun int->bit-vec (integer &key size)
  "Convert int to a simple bit vector. If size is specified will return a vector
  of that length, otherwise will return the shorted valid vector for that int."
  (let ((bit-length (or size (max 1 (ceiling (sqrt integer))))))     
    (cond ((minusp integer)
           (error "Integer must be positive."))
          ((and size (> bit-length size))
           (error "Specified size is not large enough to hold the resulting
                   vector"))
          (t (let ((array (make-array bit-length
                                     :adjustable nil
                                     :fill-pointer nil
                                     :element-type 'bit)))
               (loop for bit-index from (- bit-length 1) downto 0
                     for array-index from 0 below bit-length do
                       (setf (aref array array-index)
                             (ldb (byte 1 bit-index) integer)))
               (dbg 'hc "Converted ~A to ~A~%" integer array)
               array)))))

