(defpackage amss
  (:use :cl :graph-db))
(in-package :amss)

;;; Settings
(defvar *graph-name* :amss-graph)
(defvar *graph-path* "/home/user/AMSS/amss-graph/")
(log:config :all :sane :d :nopretty :thread :daily "/home/user/AMSS/amss-graph.log")

;;; Initialization of amss graph
(defun initialize-amss ()
  (setf *graph* (make-graph *graph-name* *graph-path* :buffer-pool-size 10000)))

(defun open-amss ()
  (setf *graph* (open-graph *graph-name* *graph-path*)))

(defun close-amss ()
  (close-graph *graph*)
  (setf *graph* nil))



;;; Types

;;; Schema


(def-vertex machine ()
  ((type :type string))
  :amss-graph)

(def-vertex generic-machine (machine)
  ()
  :amss-graph)

(def-vertex specific-machine (machine)
  ((brand :type string)
   (model :type string)
   (specs :type string))
  :amss-graph)

(def-vertex modified-machine (specific-machine)
  ((modifications :type string))
  :amss-graph)


(def-vertex part ()
  ((brand :type string)
   (part-number :type string))
  :amss-graph)


(def-vertex service ()
  ((spec :type string))
  :amss-graph)


(def-vertex schedule ()
  ()
  :amss-graph)

(def-vertex mile-schedule (schedule)
  ((miles :type number))
  :amss-graph)

(def-vertex hour-schedule (schedule)
  ((hours :type number))
  :amss-graph)


(def-edge at ()
  ()
  :amss-graph)

(def-edge at-date (at)
  ()
  :amms-graph)

(def-edge at-hour (at)
  ()
  :amss-graph)

(def-edge at-mile (at)
  ()
  :amss-graph)


(def-edge work ()
  ()
  :amss-graph)

(def-edge service (work)
  ()
  :amss-graph)

(def-edge replace (work)
  ()
  :amss-graph)

(def-edge rebuild (work)
  ()
  :amss-graph)

(def-edge inspect (work)
  ()
  :amss-graph)



;;; Queries

;;; Indexes

;;; Views


;;; Printing

;;; Data

(defun make-all-at-hour ()
  (with-transaction ()
    (loop for hour from 50 to 1000 by 50
         do (make-at-hour :hours hour))))

(defun make-all-services ()
  (let ((services '(oil trans hydraulic front-diff steering-gear battery grease clutch-pedal
                    brake-pedal toe-in axle-play air-filter-check rad-screen-coolant belt fuel
                    valves tire-pressure tire-wear switches coolant air-filter steering-hoses
                    fuel-lines rad-hoses)))
    (with-transaction ()
      (loop for service in services
           do (make-service :spec service)))))

