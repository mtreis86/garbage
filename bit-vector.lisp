(defpackage bit-vector-twiddle
  (:use :cl)
  (:nicknames :bvt))
(in-package :bit-vector-twiddle)

(defun int-to-bit-vector (integer &optional size)
  "Convert int to a simple bit vector. If size is specified will return a vector
  of that length, otherwise will return the shorted valid vector for that int."
  (let ((bit-length (or size (max 1 (+ (log integer 2) 1)))))     
    (cond ((minusp integer)
           (error "Integer must be positive."))
          ((and size (> bit-length size))
           (error "Specified size is not large enough to hold the resulting vector"))
          (t (let ((array (make-bit-vector bit-length)))
               (loop for bit-index from (- bit-length 1) downto 0
                     for array-index from 0 below bit-length do
                       (setf (aref array array-index)
                             (ldb (byte 1 bit-index) integer)))
               array)))))

(defun make-bit-vector (length)
  "Create a new bit vector, of length size, with all 0s as elements."
  (make-array length :element-type 'bit :initial-element 0))

(defun copy-bit-vector (vector)
  "Copy the bit-vector such that the new object is fully separated from the old."
  (make-array (length vector) :element-type 'bit :initial-contents vector))

(defun bit-vector-length (vector)
  (array-total-size vector))

(defun rotate-left (vector bits)
  "Rotate the vector how ever many bits over in the direction. (rotate #*00001 left 2)
  returns #*00100. Rotation rolls over the edge so (rotate #*10000 left 2) returns #*00010."
  (loop with new-vector = (make-bit-vector (bit-vector-length vector))
        for old across vector
        for index from)) ; TODO


(defun set-bit (vector index bit)
  "Set the bit in the vector at index to bit."
  (setf (aref vector index) bit))

(defun get-bit (vector index)
  (aref vector index))
