(in-package #:r-cache)

#|I have an idea for an indexed hash table I am going to make as a way to save
things to disk in a organized way that minimizes disk access. The basic idea at
this point is to have a ring buffer vector with the hash table keys as values.
As new entries are loaded into the hash, the ring revolves and the old entries
are removed. As entries are accessed they are counted and the highest priority
objects go into a second indexed hash with a sorted rather than ringed index.

A couple notes on things I am thinking about regarding this:
Three levels of storage: long-term, short-term, high-priority
Three models of storage to disk: write-all, write-delay, write-never
High priority will need either: delay, access-frequency, or flagging

Order of sub-components:
Ring buffer is a vector with a pointer to the current entry
IHash is a hash table with an index using a ring buffer
Store is some set of ihashes and a file

It may be best to make file-storage appear to be an IHash as far as the code
to access it goes, rather than having a different sructure. Or at least they
will share a large amount of the structure so I think CLOS from this point on
is the way to go.


Exposed generic methods:
  r-drop   - remove the entry from the cache
  r-clear  - remove all entries from the cache
  r-set    - add and entry to the cache
  r-get    - get a copy of an entry in the cache
  r-take   - get a copy of the entry in the cache then remove it
  r-copy   - copy the cache
  r-adjust - make the cache larger or smaller depending on needs
  r-equalp - are the caches equivalent?
  r-size   - how many entries in the cache

For now the only exported symbols from this package will be generic functions.
If anyone ever actually uses this I will likely export many other symbols.
Figure that out later, for now its easier to minimize the package size.
|#

(defclass r-cache ()
  ((size-current :initarg :size :accessor size)
   (size-original :initarg :size :reader size-original)
   (count :initform 0 :accessor r-count)
   (compactable :initarg :compactable :reader compactable)
   (compact-from-ratio :initarg :compact-from-ratio
                       :accessor compact-from-ratio)
   (compact-to-ratio :initarg :compact-to-ratio
                     :accessor compact-to-ratio)
   (expandable :initform t :initarg :expandable :reader expandable)
   (expand-from-ratio :initarg :expand-from-ratio
                      :accessor expand-from-ratio)
   (expand-to-ratio :initarg :expand-to-ratio
                    :accessor expand-to-ratio))
  (:documentation
    "A cache is a generic storage unit with specialized sub-types."))


;;; Exposed generics
(defgeneric r-drop (r-cache key)
  (:documentation "Remove the entry from the cache."))
(defgeneric r-clear (r-cache)
  (:documentation "Remove all entries from the cache."))
(defgeneric r-set (r-cache key &rest value)
  (:documentation "Add an entry to the cache. If the cache is a ring-buffer,
  the key is the entry and value is ignored."))
(defgeneric r-get (r-cache key)
  (:documentation "Get a copy of an entry in the cache."))
(defgeneric r-take (r-cache key)
  (:documentation "Get a copy of the entry in the cache then remove it."))
(defgeneric r-copy (r-cache)
  (:documentation "Copy the cache."))
(defgeneric r-adjust (r-cache)
  (:documentation "Make the cache larger or smaller depending on needs."))
(defgeneric r-size (r-cache)
  (:documentation "How many entries in the cache?"))
(defgeneric r-equalp (r-cache-1 r-cache-2)
  (:documentation "Are the two caches equivalent to eachother as in equalp?"))

;;; Hidden generics
(defgeneric expand (r-cache)
  (:documentation "Make the cache larger if needed."))
(defgeneric compact (r-cache)
  (:documentation "Make the cache smaller if needed."))

;;; Ring buffer

(defclass ringb (r-cache)
  ((pointer :initarg :pointer :accessor ringb-pointer)
   (vector :initarg :vector :accessor ringb-vector))
  (:documentation
   "A ring buffer is a cache made from a vector with a pointer at the current
   location in the buffer being worked on."))


(defun make-ringb (size &key (compactable t) (compact-from-ratio 1/10)
                          (compact-to-ratio 1/2) (expandable t)
                          (expand-from-ratio 9/10) (expand-to-ratio 1/2))
  "Make a new ring buffer of the size."
  (make-instance 'ringb :pointer 0 :size size
                        :vector (make-array size :initial-element nil)
                        :compactable compactable
                        :compact-from-ratio compact-from-ratio
                        :compact-to-ratio compact-to-ratio
                        :expandable expandable
                        :expand-from-ratio expand-from-ratio
                        :expand-to-ratio expand-to-ratio))

;;; Exposed specialized

(defmethod r-drop ((ringb ringb) position)
  (rem-ringb ringb position))

(defmethod r-clear ((ringb ringb))
  (loop :for index :from 0 :below (size ringb)
        :for entry :across (ringb-vector ringb)
        :when entry :do (rem-ringb ringb index))
  (setf (ringb-pointer ringb) 0))

(defmethod r-set ((ringb ringb) key &rest value)
  (declare (ignore value))
  (setf (rbref ringb) key))

(defmethod r-get ((ringb ringb) position)
 (rbref ringb position))

(defmethod r-take ((ringb ringb) position)
  (let ((value (r-get ringb position)))
    (r-drop ringb position)
    value))

(defmethod r-copy ((ringb ringb)))

(defmethod r-adjust ((ringb ringb))
  (compact-ringb ringb)
  (expand-ringb ringb)
  ringb)

(defmethod r-size ((ringb ringb))
  (size ringb))

(defmethod r-equalp ((ringb1 ringb) (ringb2 ringb))
  (and (= (ringb-pointer ringb1)
          (ringb-pointer ringb2))
       (= (size ringb1)
          (size ringb2))
       (= (size-original ringb1)
          (size-original ringb2))
       (equalp (ringb-vector ringb1)
               (ringb-vector ringb2))
       (eq (compactable ringb1)
           (compactable ringb2))
       (= (compact-from-ratio ringb1)
          (compact-from-ratio ringb2))
       (= (compact-to-ratio ringb1)
          (compact-to-ratio ringb2))
       (eq (expandable ringb1)
           (expandable ringb2))
       (= (expand-from-ratio ringb1)
          (expand-from-ratio ringb2))
       (= (expand-to-ratio ringb1)
          (expand-to-ratio ringb2))))


;;; Hidden specialized

(defmethod expand :around ((ringb ringb))
  ;; Only expand if both expandable and necessary.
  (dbg :cache "Checking ~A to see if it needs expanding.~%" ringb)
  (when (and (expandable ringb)
             (> (/ (r-count ringb) (size ringb))
                (expand-from-ratio ringb)))
    (call-next-method)))

(defmethod expand ((ringb ringb))
 "Expand an amount that results in having the number of entries
   relative to the expand-to-ratiob."
  (dbg :cache "Expanding ~A~%" ringb)
  (let ((new-size (floor (r-count ringb) (expand-to-ratio ringb))))
    (setf (ringb-vector ringb) (adjust-array (ringb-vector ringb)
                                             new-size :initial-element nil)
          (size ringb) new-size)
    (dbg :cache "~A expanded to ~A~%" ringb new-size))
  ringb)

(defmethod compact :around ((ringb ringb))
  ;; Only compact if compactable and necessary.
  (dbg :cache "Checking ~A to see if it needs compacting.~%" ringb)
  (when (and (compactable ringb)
             (< (/ (r-count ringb) (size ringb))
                (compact-from-ratio ringb)))
    (call-next-method)))

(defmethod compact ((ringb ringb))
  "Move all entries to the beginning of the ring and set the
  pointer to the first empty entry."
  (dbg :cache "Compacting ~A~%" ringb)
  (let ((new-size (ceiling (* (compact-to-ratio ringb) (size ringb)))))
    (multiple-value-bind (vector pointer size)
        (compact-vector (ringb-vector ringb) new-size)
      (setf (ringb-vector ringb) vector
            (ringb-pointer ringb) pointer
            (size ringb) size))
    (dbg :cache "~A compacted to ~A~%" ringb new-size))
  ringb)


;;; Hidden direct

(defmethod rbref ((ringb ringb) &optional position)
   "Return the value at the given position of the ring buffer. If no position
   is given, use the current one."
  (svref (ringb-vector ringb) (if position position (ringb-pointer ringb))))

;; (defmethod (setf rbref) :before (args (ringb ringb))
;;   ;; Adjust the ringb before setting
;;   (when (and (or (compact-ringb ringb)
;;                  (expand-ringb ringb))
;;              (not (null (rbref ringb))))
;;     (adjust-ringb ringb)))


(defmethod (setf rbref) (args (ringb ringb))
 "Set the current position of the ring buffer to the value then move to the
  next position."
  (dbg :cache "Setting current entry in ~A to ~A~%" ringb args)
  (setf (svref (ringb-vector ringb) (ringb-pointer ringb)) args)
  (next-ringb ringb)
 (incf (r-count ringb)
  ringb))


(defmethod next-ringb ((ringb ringb))
  "Advance the ring buffer one entry."
  (if (= (ringb-pointer ringb)
         (- (size ringb) 1))
      (setf (ringb-pointer ringb) 0)
      (incf (ringb-pointer ringb)))
  (ringb-pointer ringb))

(defmethod get-next-ringb ((ringb ringb))
  "Get the next entry in the ring buffer. Doesn't move the pointer.")

(defmethod get-prev-ringb ((ringb ringb))
  "Get the previous entry in the ring buffer. Doesn't move the pointer.")

(defmethod rem-ringb ((ringb ringb) &optional position)
  "Remove the entry from the ring buffer at the given position if given. If
  not given remove the current position entry."
  (setf (svref (ringb-vector ringb) (if position position
                                        (ringb-pointer ringb)))
        nil)
  (decf (r-count ringb)))

(defmethod count-entries ((ringb ringb))
  "Return the number of filled entries in the ring buffer."
  (loop :for entry :across (ringb-vector ringb)
        :counting entry))



;;; Indexed hash table

(defclass ihash (r-cache)
  ((ringb :initarg :ringb :accessor ihash-ringb)
   (hash :initarg :hash :accessor ihash-hash)
   (flags :initarg :flags :reader ihash-flags))
  (:documentation
   "An ihash is a hash table with a ring buffer as an index."))

(defun make-ihash (&key (initial-size 100) (test 'eql))
  "Make an indexed hash table, a hash table with an index as a ring buffer."
  (make-instance 'ihash
                 :ringb (make-ringb initial-size)
                 :ihash (make-hash-table :test test)))

;;; Exposed specialized

(defmethod r-drop (ihash key))
(defmethod r-clear (ihash))
(defmethod r-set (ihash key &rest value))
(defmethod r-get (ihash key))
(defmethod r-take (ihash key))
(defmethod r-copy (ihash))
(defmethod r-adjust (ihash))
(defmethod r-size (ihash))
(defmethod r-equalp (ihash-1 ihash-2))

;;; Hidden specialized

(defmethod r-expand (ihash))
(defmethod r-compact (ihash))

;;; Hidden direct

(defmethod get-ihash (key (ihash ihash))
  "Take a key and an indexed hash table and return the value associated with
  the key."
  (gethash key (ihash-hash ihash)))

(defmethod (setf get-ihash) (key (ihash ihash) value)
  "Take a key, an indexed hash table, and a value. Set the key to the value and
  update the ring buffer to match."
  `(progn (setf (gethash ,key (ihash-hash ,ihash))
                (cons ,value (ringb-pointer (ihash-index ,ihash))))
          (set-ringb (ihash-index ,ihash) ,key)))

(defmethod drop-ihash ((ihash ihash) key)
  "Remove the key from the hash-table and the index."
  (let ((buff-position (first (get-ihash key ihash))))
    (remhash key (ihash-hash ihash))
    (rem-ringb (ihash-ringb ihash) buff-position)))

(defmethod clear-ihash ((ihash ihash))
  "Remove all entries from both the hash table and the index."
  (loop :for entry :across (ringb-vector (ihash-ringb ihash))
        :when entry :do (drop-ihash ihash entry))
  (clear (ihash-ringb ihash)))








(defclass store (r-cache)
  ((long-term :initarg :long-term :accessor store-long)
   (short-term :initarg :short-term :accessor store-short)
   (high-priority :initarg :high-priority :accessor store-priority)
   (levels :initarg :levels :reader store-levels)
   (model :initarg :model :reader store-model))
  (:documentation
   "A store is an ihash that can write to disk."))







(defun vector-swap (vector pos1 pos2)
  "Swap the entries in position1 and position2 of the vector."
  (let ((tmp (svref vector pos1)))
    (setf (svref vector pos1) (svref vector pos2)
          (svref vector pos2) tmp)))

(defun compact-vector (vector new-size)
  "Shift the contents of the vector to the left so there are no more empty
  entries in between entries. Return new vector, the pointer to the last filled
  element, and the new-size of the vector. Will error if there are too many
  entries to achieve that new-size."
  (let ((new-vector (make-array new-size :initial-element nil)))
    (loop :with new-index = 0
          :for entry across vector
          :when (not (null entry))
            :do (setf (svref new-vector new-index) entry)
                (incf new-index)
          :finally (return (values new-vector new-index new-size)))))





;; (defmethod r-drop ())
;; (defmethod r-clear ())
;; (defmethod r-set ())
;; (defmethod r-get ())
;; (defmethod r-take ())
;; (defmethod r-copy ())
;; (defmethod r-adjust ())
;; (defmethod r-size ())
;; (defmethod r-equalp ())
