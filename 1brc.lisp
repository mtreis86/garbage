(defpackage 1brc
  (:use :cl))
(in-package :1brc)

(defparameter *file* #P"~/common-lisp/1brc/1brc.txt")

(defparameter *total* (make-hash-table :test #'equalp))

(defstruct temp mean count min max)

(defun run ()
  "Open the file, line by line update the total."
  (with-open-file (stream *file*)
    (loop for line = (read-line stream nil)
          while line
          do (multiple-value-bind (city temp)
                 (parse-line line)
               (update-total city temp)))))

(defun update-total (city temp)
  "Update the total for the given city. Totals is a hash table, each city is stored
by string as the key. The value is a struct of the min max mean and count of
entries for that city. This updates one entry in the table with a new reading."
  (let ((entry (gethash city *total*)))
    (if entry
        (progn
          (when (< temp (temp-min entry))
            (setf (temp-min entry) temp))
          (when (> temp (temp-max entry))
            (setf (temp-max entry) temp))
          (setf (temp-mean entry)
                (+ (/ (* (temp-mean entry)
                         (temp-count entry))
                      (1+ (temp-count entry)))
                   (/ temp (1+ (temp-count entry)))))
          (incf (temp-count entry)))
        (setf (gethash city *total*)
              (make-temp :mean temp :count 1 :min temp :max temp)))))

(defun parse-line (line)
  "Each line is of the format city;temp where city is in ASCII, then ; as a delim,
then the temp in decimal float with one position after the decimal. This takes
in that string and returns the city and temp in values as a string and float."
  (let ((pos (position #\; line)))
    (values (subseq line 0 pos)
            (parse-float (subseq line (1+ pos))))))

(defun parse-float (string)
  "Parse-float parses string into a float. Note, requires one and only one digit past the decimal. Will work on negative numbers."
  (let* ((pos (position #\. string))
         (base (float (parse-integer (subseq string 0 pos))))
         (dec (float (/ (parse-integer (subseq string (1+ pos)))
                        10))))
    (if (plusp base)
        (+ base dec)
        (- base dec))))
