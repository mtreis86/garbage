(defpackage closure-net
  (:use :cl))
(in-package :closure-net)

;;;; Closure Network Tools by Michael Reis

#|
The goal of this project is to provide the underlying tooling for abstracting over a closure
network. The main benefit of a network such as this is that updates for a portion of the network
automatically propagate to the appropriate nodes without having to make any determinations as to
which need updating.

Data structure wise, a closure network is similar to a cons tree. The main difference being the
closure network captures additional information. Like a binary tree the network can be walked.
Just because we can update nodes doesn't mean we have to.
|#

(defun make-cn ())

(defun update-cn ())

(defun update-node ())

(defun add-node ())

(defun del-node ())












