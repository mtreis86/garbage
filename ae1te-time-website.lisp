(defpackage ae1te
  (:use :cl :hunchentoot :cl-who :cl-fad)
  (:import-from :mtr :with-gensyms))
(in-package :ae1te)

(defun ae1te ()
  (hunchentoot:start (make-instance 'hunchentoot:easy-acceptor :port 8080)))

(defvar *http-stream* *standard-output*)

(define-easy-handler (index :uri "/index.html" :default-request-type :post)
    ()
  (with-html-output-to-string (*http-stream*)
    (:html
     (:head (:title "AE1TE"))
     (:body
      (:h2 (main-link))
      (:h3 (links '(("/time.html" . "Time")
                    ("/log.html" . "Log"))))))))

(define-easy-handler (time-page :uri "/time.html" :default-request-type :post)
    ()
  (with-html-output-to-string (*http-stream*)
    (:html
     (:head (:title "AE1TE Time Utilities"))
     (:body
      (:h2 (main-link))
      (table-2 "Time Utilities: "
               (list (cons "Current time is: " (print-current-time nil))))))))

(defmacro with-html (&body body)
  `(with-html-output (*http-stream*)
     ,@body))

(defun main-link ()
  (with-html
    (:a (:hr
         (:a :href "/index.html" "AE1TE")))))

(defun link (link text)
  (with-html
    (:a :href link
        (:b (str text)))
    :br))

(defun links (link-text-list)
  (with-html
    (loop for (link . text) in link-text-list
       do (link link text))))
(defun table-2 (title rows)
  (with-html
    (:p (:table :border 1 :cellpadding 2 :cellspacing 0
                (:tr (:td :colspan 2 (str title)))
                (loop for (first . rest) in rows
                     do (htm (:tr (:td (str first) (str rest)))))))))

(defmacro with-time (source &body body)
    `(multiple-value-bind (second minute hour date month year day-of-week dst-p zone)
         (decode-universal-time ,source)
       (declare (ignorable second minute hour date month year day-of-week dst-p zone))
       (let ((day-names '("Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Saturday" "Sunday")))
         (setf day-of-week (nth day-of-week day-names))
         ,@body)))

(defun print-time (&optional (stream t) (source (get-universal-time)))
  (with-time source
    (format stream "~2,'0d:~2,'0d:~2,'0d of ~a, ~d/~2,'0d/~d (GMT~@d)"
            hour minute second day-of-week month date year (- zone))))

(defun print-current-time (&optional (stream t))
  (print-time stream (get-universal-time)))

(defun under-construction ()
  (with-html
    (:h1 "Page under construction, check back later.")))

(define-easy-handler (log-page :uri "/log.html" :default-request-type :post)
    (card name phone email website)
  (with-html-output-to-string (*http-stream*)
    (:html
     (:head (:title "AE1TE Log"))
     (:body
      (:h2 (main-link))
      (:h3 "Guest Log")
      ;; insert guest log form here
      (:table :border 1 :cellpadding 2 :cellspacing 0
              (input-row "Name: " name (or name "name"))
              (input-row "Phone: " phone (or phone "phone"))
              (input-row "Email: " email (or email "email"))
              (input-row "Website: " website (or website "website"))
              (:tr
               (:td :colspan 2
                    (:input :type "submit"))))
      (when (plusp (current-id))
        (loop for id from 0 to (current-id)
           do (print-html-card (get-card id)))))))
  (setf card (make-card name phone email website)))

(defclass card ()
  ((id :initform (next-id) :reader id)
   (name :initarg :name :initform "no one" :accessor name)
   (phone :initarg :phone :initform "555-1234" :accessor phone)
   (email :initarg :email :initform "no@one.com" :accessor email)
   (website :initarg :website :initform "http://website.com" :accessor website)))

(let ((id 0)
      (cards (make-array 10 :adjustable t :fill-pointer 0)))
  (defun save-card (card) (setf (aref cards (id card)) card))
  (defun get-card (id) (aref cards id))
  (defun next-id () (incf id))
  (defun current-id () id)
  (defun reset-id () (setf id 0)))

(defun make-card (name phone email website)
  (loop for entry in '(name phone email website)
       with valid = t
     unless (validate entry)
       do (setf valid nil)
     while valid
       finally (return valid))
  (make-instance 'card :name name :phone phone :email email :website website))

(defclass contact () ())
(defclass name (contact) ())
(defclass phone (contact) ())
(defclass email (contact) ())
(defclass website (contact) ())

(defgeneric validate (contact))

(defmethod validate ((contact contact))
  (stringp contact))

(defun print-html-card (card)
  (table-2 (format nil "Contact: ~A" (name card))
           (list (cons "Phone: " (phone card))
                 (cons "Email: "  (email card))
                 (cons "Website: " (website card)))))

(defun text-input (name value)
  (with-html
    (:input :type 'text :name name :value value)))

(defun input-row (printed name value)
  (with-html
    (:tr
     (:td (str printed))
     (:td (text-input name value)))))



















