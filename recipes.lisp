(defpackage recipes
  (:use :cl))
(in-package :recipes)

;;; *ingredients* is a hashtable with symbols for the keys and default unit for the values
;;; the unit will be used for measurement of the ingredient
(defvar *ingredients* (init-ingredients))

(defun init-ingredients ()
  "Set up the default ingredients."
  ;; this is hard coded now. could be persistent instead
  (let ((ingredients '((flour . g) (salt . mg) (water . cc) (yeast . g)))
        (table (make-hash-table)))
    (mapcar (lambda (ing)
              (setf (gethash (car ing) table)
                    (cdr ing)))
            ingredients)
    table))

(defun known (ingredient)
  (and (gethash ingredient *ingredients*) t))

(defun make-ingredient (ingredient unit)
  (setf (gethash ingredient *ingredients*) unit))


;;; *stock* is a hashtable containing symbols of ingredients as keys and
;;; integer quantity in the unit for that ingredient, as the value

(defvar *stock* (make-stock))

(defun make-stock ()
  (make-hash-table))

(defun in-stock (ingredient)
  (gethash ingredient *stock*))

(defun adjust-stock (ingredient delta)
  (let ((quantity (in-stock ingredient)))
    (if quantity
        (set-stock ingredient (+ quantity delta))
        (error "Ingredient not in stock: ~A." ingredient))))

(defun set-stock (ingredient quantity)
  (assert (typep quantity 'integer))
  (setf (gethash ingredient *stock*) quantity))

(defun available-ingredients ()
  (let ((available nil))
    (maphash (lambda (ingredient quantity)
               (when (plusp quantity)
                 (push ingredient available)))
             *inventory*)
    available))


;;; *recipes* is a hashtable of tags that are bidirectional pointers to each recipe

(defvar *recipes* (init-recipes))

;; (defun init-recipes ()
;;   (labels ((save-recipe (recipe table)))
;;             ; TODO
;;     (let ((table (make-hash-table)))
;;          (recipe (make-recipe)) ;TODO
;;       (save-recipe recipe table)
;;       table)))

#|
a recipe is:

quantitatives: hash table with symbols mapped to times temps and other measurements
-- note that since the instances of the symbols need to be unique we'll have to
-- do something like intern some gensyms or something

qualitatives: a directed acyclic graph with quantitatives as nodes and functions on them as edges
-- note I'd prefer a simpler structure

tags: hash table of keysyms used for recipe organization
|#

(defstruct recipe
  quants
  quals
  tags)

#|
Okay so this is where it gets fun
up to now everything has been sort of obvious
but the next step is exploratory

how do we want this to work?

lets go through and make a chili with some home made bread bowl as a quick but complex recipe

so what is it from the top? we have a series of abstractions
top most layer is the meal of bread & chili
then both bread and chili have their own layer
bread is a mix of ingredients, some of which like yeast can be a layer
and chili has several layers itself
some of those layers are stages through time though so I dunno if a DAG is avoidable
but we can abstract that away itself I guess

lets lay this out in some form of arrow notation:

(-> chili-bread-bowl
    bread-bowl
    chili
    toppings)

(-> bread-bowl
    bread
    bowlify)

(-> chili
    slow-cooked
    mid-cooked
    fast-cooked)

(-> slow-cooked
    browned-meat
    onion
    garlic
    seasoning
    pepper)

(-> mid-cooked
    onion
    tomato
    pepper)

(-> fast-cooked
    onions
    peppers
    beans)

That sorta works for laying out the idea of what a chili is. But there are issues here.
For one there are no optionals. Chili is really more flexible than this
Could almost use CLOS for this.
In fact this could be a decent intro to CLOS, specifically the before/after methods
We could basically have the recipe defined as a series of methods where optional stuff is done before or after.
I'm not sure what I don't like about that.


Lets back up a minute. What are the top most level inputs and outputs?

The input would be a list of ingredients, going into a recipe builder that will generate the
right recipe given enough input information.

The output would be that recipe in a human readable format.


So lets work backwards for a bit. Mazes are bidirectional after all. Start and finish are relative.

Imagine we have a recipe builder. We build a yeast recipe.
Yeast is simple, its yeast water and food
The water is to rehydrate
The food increases the volume but has an optimal ratio range to the yeast
The yeast is basically the input measurement for this recipe, everything else is calculated
The tricky bit then is time
Calculus then? We'll see.


Anyways, I think this form can work...

(defrecipe yeast (yeast)
  (-> yeast
      feed
      hydrate)
  (-> feed
      over-time
      within-range
      (add food))
  (-> hydrate
      over-time
      within-range
      (add water)))


Basic idea is the arrow lists are stages feeding the next
(-> yeast
    feed
    hydrate)

unrolls into

(hydrate (feed yeast))

and is equivalent to

(let (yeast)
  (setf yeast (feed yeast))
  (setf yeast (hydrate yeast))
  yeast)


if the stage is a symbol we treat it like a function call with one arg being the previous stage.
if the stage is a list then we treat the list as the function call with the previous stage as
the final arg

(-> hydrate
    over-time
    within-range
    (add water))

unrolls into

(add water (within-range (over-time hydrate)))


So then combining these two

((-> yeast
     feed
     hydrate)
 (-> hydrate
     over-time
     within-range
     (add water)))

unrolls into

(defun yeast (yeast)
  (add water (within-range (over-time (feed yeast)))))


But there are a few things that need to be done as well
for one there may be things in here that aren't defined somewhere and that may be on purpose

I think we'll need a pattern matcher. Finally an excuse to refactor mine. For now I can just use it.


Okay so within-range and over-time, lets think about those. They should do something like check
the quantitative that matches. In this case over-time should make something unroll into a loop.
Within range should be a loop too. Or both are directives for the looping. IRQ?
I guess adding water is happening over time as well.
Ah there should be a conditional in here somewhere. Is the yeast from before fresh?
If not fresh then the recipe timing is instantaneously now. How should that be declared?
















|#
