(defpackage bst
  (:use :cl)
  (:export #:make-bst
           #:query-bst
           #:insert-bst
           #:delete-bst))
(in-package :bst)

(defstruct bst
  root)

(defstruct node
  data left right parent)

(defmethod print-object ((node node) (stream t))
  (format stream "~&Node: ~A~&"
          (node-data node)))

(defun query-bst (data bst)
  "Query the tree for the data."
  (query-node data (bst-root bst)))

(defun query-node (data node)
  "Query this branch of the tree for the data."
  (let ((node-data (node-data node)))
    (cond ((= data node-data)
           node)
          ((and (< data node-data) (node-left node))
           (query-node data (node-left node)))
          ((and (> data node-data) (node-right node))
           (query-node data (node-right node)))
          (t nil))))

(defun insert-bst (data bst)
  "Insert the data into the tree."
  (if (null (bst-root bst))
      (setf (bst-root bst) (make-node :data data))
      (insert-node data (bst-root bst) (bst-root bst))))

(defun insert-node (data node parent)
  "Insert the data into this branch of the tree, connecting it up to the parent branch."
  (let ((node-data (node-data node)))
    (cond ((null node-data)
           (setf (node-data node) data
                 (node-parent node) parent))
          ((= data node-data)
           (return-from insert-node nil))
          ((< data node-data)
           (if (null (node-left node))
               (setf (node-left node) (make-node :data data :parent node))
               (insert-node data (node-left node) node)))
          ((> data node-data)
           (if (null (node-right node))
               (setf (node-right node) (make-node :data data :parent node))
               (insert-node data (node-right node) node)))
          (t (error "Incomparable data")))))

(defun delete-bst (data bst)
  "Remove this data from the tree."
  (let ((node (query-bst data bst)))
    ; (when node (delete-node node bst))))
    (cond ((and node (eq node (bst-root bst))) ; node exists and is root
           (rotate-root bst))
          (node ; node exists
           (delete-node node))
          (t ; node doesn't exist
           nil))))


(defun move-node (node target)
  "Move the node to take target's place."
  (let ((parent (node-parent target)))
    (if (eq target (node-left parent))
        (setf (node-left parent) node ; target is on the left of its parent
              (node-parent node) parent)
        (setf (node-right parent) node ; target is on the right of its parent
              (node-parent node) parent))))

(defun set-node-as-root (node bst)
  "Set the node as root of the bst."
  (setf (bst-root bst) node
        (node-parent node) nil))

(defun left-leaf (node)
  "Return the leaf that branches most to the left of node."
  (cond ((node-left node)
         (left-leaf (node-left node)))
        ((node-right node)
         (node-right node))
        (t node)))

(defun right-leaf (node)
  "Return the leaf that branches most to the right of node"
  (cond ((node-right node)
         (right-leaf (node-right node)))
        ((node-left node)
         (right-leaf (node-left node)))
        (t node)))

(defun left-most-branch (node)
  "Return the left most branch under the node, may or may not be a leaf."
  (if (node-left node)
      (left-most-branch node)
      node))

(defun right-most-branch (node)
  "Return the right most branch under the node, may or may not be a leaf."
  (if (node-right node)
      (right-most-branch node)
      node))

(defun leafp (node)
  "Is node a leaf?"
  (and (null (node-left node)) (null (node-right node))))

(defun open-branch-p (node)
  "Does node have at least one empty child?"
  (or (null (node-left node)) (null (node-right node))))

(defun verify-node-to-parent (node)
  "Verify that the node is less or greater than the parent depending on the location."
  (let ((parent (node-parent node)))
    (cond ((and parent (eq node (node-left parent))) ; node is left of parent
           (< (node-data node) (node-data parent)))
          ((and parent (eq node (node-right parent))) ; node is right of parent
           (> (node-data node) (node-data parent)))
          ((null parent) ; node is root
           t)
          (t ; every other case is a broken node
           nil))))

(defun next-left-branch (node)
  "Find the next branch on the left that has an open child."
  (if (open-branch-p node)
      node
      (next-left-branch (node-left node))))

(defun next-right-branch (node)
  "Find the next branch on the right that has an open child."
  (if (open-branch-p node)
      node
      (next-right-branch (node-right node))))

(defun delete-node (target)
  "Remove the target node by erasing it, if it is a leaf. Or by rotating children under it up."
  (cond ((and (leafp target) (eq target (node-left (node-parent target)))) ; target is left leaf
         (setf (node-left (node-parent target)) nil))
        ((and (leafp target) (eq target (node-right (node-parent target)))) ; target is right leaf
         (setf (node-right (node-parent target)) nil))
        (t ; target isn't a leaf, must rotate children up
         (let ((node (cond ((node-left target) ; start with left branch
                            (next-left-branch target))
                           ((node-right target) ; left branch doesn't exist
                            (next-right-branch target)))))
           (rotate-up node target)))))

(defun rotate-up (node target)
  "Rotate the node up until it takes the place of target."
  (let ((parent (node-parent node)))))










;; (defun delete-node (node bst)
;;   "Remove this node from the tree, preserving branches below. If node is only one left, stop and return the tree."
;;   (when node
;;     (let ((parent (node-parent node)))
;;       (cond ((null parent) ; node is root
;;              (rotate-root bst))
;;             ((and (null (node-right node)) (null (node-left node))) ; node is a leaf, just erase it
;;              (if (eq node (node-left parent))
;;                  (setf (node-left parent) nil)
;;                  (setf (node-right parent) nil)))
;;             ((or (null (node-right node)) (null (node-left node))) ; node has only one child, move it up
;;              (if (null (node-left node))
;;                  (move-node (node-right node) node)
;;                  (move-node (node-left node) node)))
;;             ((eq node (node-left parent)) ; node isn't a leaf so rotate child node up
;;              (rotate-left node))
;;             (t (rotate-right node))))))

;; (defun verify-bst (bst) ; TODO write TCO version
;;   "Walks through the tree confirming that none of the branches is out of order within the parent.
;;   In a tree with only insertions this isn't needed, but with deletions it is useful as a test.
;;   Warning, not tail call optimized, may crash, use for testing only."
;;   (labels ((verify-node (node)
;;              (let ((left (node-left node))
;;                    (right (node-right node)))
;;                (and (verify-node-to-parent node)
;;                     (or (null left) (verify-node left))
;;                     (or (null right) (verify-node right))))))
;;     (verify-node (bst-root bst))))

;; (defun rotate-left (target)
;;   "Rotate the left branches below the target upwards until the target is removed."
;;   (let ((leaf (left-leaf target)))
;;     (labels ((move-node-up (node)
;;                (unless (eq node target)
;;                  (let* ((parent (node-parent node))
;;                         (parent-right (node-right parent)))
;;                    (when (eq target parent)
;;                      (let ((parent-parent (node-parent parent)))
;;                        (setf (node-left parent-parent) node
;;                              (node-parent node) parent-parent)))
;;                    (when parent-right

;; (defun rotate-right (target)
;;   "Rotate the right branches below the target upwards until the target is moved."
;;   (let ((leaf (right-leaf target)))
;;     (labels ((move-node-up (node)
;;                (unless (eq node target)
;;                  (let* ((parent (node-parent node))
;;                         (parent-left (node-left parent)))
;;                    (when (eq target parent)
;;                      (let ((parent-parent (node-parent parent)))
;;                        (setf (node-right parent-parent) node
;;                              (node-parent node) parent-parent)))
;;                    (when parent-left
;;                      (setf (node-left node) parent-left
;;                            (node-parent parent-left) node))
;;                    (move-node-up parent)))))
;;       (move-node-up leaf)))
;;   (rotate-left (node-left target)))

;; (defun rotate-root (bst)
;;   "Rotate the left (or right if no left exists) branch of the root up to become the new root."
;;   ;; Since we can rotate either left or right nodes up we pick left because zero is closer to
;;   ;; the middle of any computable set than infinity
;;   ;; I don't like this but it is simpler than baking it into rotation and keeps that fast
;;   (let* ((root (bst-root bst))
;;          (right (node-right root))
;;          (left (node-left root)))
;;     (cond ((and (null left) (null right)) ; node is last node in tree
;;            (setf (bst-root bst) nil))
;;           ((null left) ; left doesn't exist, make right root
;;            (make-root right bst))
;;           ((null right) ; right doesn't exist, make left root
;;            (make-root left bst))
;;           (t (let ((new-left (node-left left))) ; left and right exist, move left up as root
;;                (rotate-left left)
;;                (make-root left bst)
;;                (setf (node-parent new-left) left
;;                      (node-right left) right
;;                      (node-parent right) left))))))
;;                      (setf (node-right node) parent-right
;;                            (node-parent parent-right) node))
;;                    (move-node-up parent)))))
;;       (move-node-up leaf))))










